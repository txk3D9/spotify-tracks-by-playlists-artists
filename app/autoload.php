<?php
/**
 * @param $class
 * @return bool
 * @throws Exception
 */
function loadClass($class) {

    $paths = ['Models', 'Utilities', 'Views'];

    foreach($paths as $path) {
        $file = dirname(__FILE__).'/'.$path.'/'.$class.'.php';
        if(file_exists($file)) {
            require_once $file;

            return true;
        }
    }

    throw new \Exception('Class '.$class.' could not be loaded');
}


spl_autoload_register('loadClass');