<?php

class TrackUtility
{

    public static function getByPlaylistId($playlistId)
    {
        $tracks = [];
        $url = 'https://api.spotify.com/v1/playlists/'.$playlistId.'/tracks';
        do {
            $response = CurlUtility::doRequest($url);
            $tracks = array_merge($tracks, $response['items']);
            $url = array_key_exists('next', $response) ? $response['next'] : null;
        } while ($url);

        return $tracks;
    }
}